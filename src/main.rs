use clap::Parser;
use serde::Deserialize;
use serde_yaml::Value;
use std::collections::BTreeMap as Map;
use std::env;
use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;
use walkdir::WalkDir;

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    #[arg(short, long)]
    list: bool,
    #[arg(long)]
    branch_group: String,
}

#[derive(Deserialize, Default)]
pub struct GitlabCi {
    pub include: Option<Vec<String>>,
}

#[derive(Deserialize)]
struct MetaData {
    repopath: String,
    repoactive: bool,
}

#[derive(Deserialize)]
pub struct KdeCi {
    #[serde(rename = "Dependencies")]
    pub dependencies: Option<Vec<Dependencies>>,
}

#[derive(Deserialize)]
pub struct Dependencies {
    pub on: Vec<String>,
    pub require: Require,
}

#[derive(Deserialize)]
pub struct Require {
    #[serde(flatten)]
    pub other: Map<String, Value>,
}

// The output is wrapped in a Result to allow matching on errors
// Returns an Iterator to the Reader of the lines of the file.
pub fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where
    P: AsRef<Path>,
{
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}

fn get_repo_metadata() -> Map<String, MetaData> {
    let mut result = Map::new();

    for entry in WalkDir::new(format!(
        "{}/projects-invent",
        &env::var("REPO_METADATA").unwrap()
    ))
    .into_iter()
    .filter_map(|e| e.ok())
    {
        let f_name = entry.file_name().to_string_lossy();

        if f_name.ends_with(".yaml") {
            if entry.path().to_str().unwrap().contains("websites") {
                continue;
            }

            if entry.path().to_str().unwrap().contains("wikitolearn") {
                continue;
            }

            if entry.path().to_str().unwrap().contains("webapps") {
                continue;
            }

            if entry.path().to_str().unwrap().contains("historical") {
                continue;
            }

            if entry.path().to_str().unwrap().contains("documentation") {
                continue;
            }

            if entry.path().to_str().unwrap().contains("sysadmin/") {
                continue;
            }

            if entry.path().to_str().unwrap().contains("neon") {
                continue;
            }

            if entry.path().to_str().unwrap().contains("packaging") {
                continue;
            }

            let md: MetaData =
                serde_yaml::from_reader(std::fs::File::open(entry.path()).unwrap()).unwrap();

            let key = &md.repopath;

            if !md.repoactive {
                continue;
            }

            result.insert(key.to_string(), md);
        }
    }

    return result;
}

fn project_uses_hybrid(base_name: &str) -> Option<bool>
{
    let kdeci_file = std::fs::File::open(format!(
            "{}/{}/.kde-ci.yml",
            &env::var("REPOS").unwrap(),
            base_name
        ));

        let gitlabci_file = std::fs::File::open(format!(
            "{}/{}/.gitlab-ci.yml",
            &env::var("REPOS").unwrap(),
            base_name
        ));

        if gitlabci_file.is_err() {
            return None;
        }

        let gitlabci_: Result<GitlabCi, _> = serde_yaml::from_reader(gitlabci_file.unwrap());

        if gitlabci_.is_err() {
            return None;
        }

        let gitlabci = gitlabci_.unwrap();

        if gitlabci.include.is_none() {
            return None;
        }

        let qt6 = gitlabci.include.unwrap().iter().any(|line| line.contains(&String::from("linux-qt6")));

        if !qt6 {
            return None;
        }

        if kdeci_file.is_err() {
            return None;
        }

        let kdeci: KdeCi = serde_yaml::from_reader(kdeci_file.unwrap()).unwrap();

        if kdeci.dependencies.is_none() {
            return None;
        }

        let deps = kdeci.dependencies.unwrap();

        for d in deps {

            let relevant_platform = d.on.iter().any(|platform| platform == "Linux" || platform == "Linux/Qt6" || platform == "@all");

            if !relevant_platform {
                continue;
            }

            for i in d.require.other {

                let name = i.0;
                let branch = i.1;

                if name.starts_with("third-party/") {
                    continue;
                }

                if !name.starts_with("frameworks/") {
                    continue;
                }

                if name == "frameworks/extra-cmake-modules" {
                    continue;
                }

                if branch == "@latest" || branch == "@stable" {
                    return Some(false)
                }
            }
        }

        return Some(true);
}

fn main() {
    let metadata = get_repo_metadata();

    let all_projects: Vec<String> = metadata.iter().map(|(key, _)| key.to_string()).collect();

    for p in all_projects {
        let base_name = p.split('/').nth(1).unwrap();

        let result = project_uses_hybrid(base_name);

        if result.is_some() && !result.unwrap() {
            println!("{}", base_name);
        }
    }
}
